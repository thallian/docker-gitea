[Gitea](https://gitea.io/) server.

# Ports
- 80

# Volumes
- /home/gitea/repositories
- /home/gitea/data/lfs
- /home/gitea/data/attachments
- /home/gitea/data/avatars
- /home/gitea/data/ssh

# Environment Variables
## SMTP_HOST
Smtp host sued to send emails.

## SMTP_FROM
Mail from address.

## SMTP_USER
User for smtp authentication.

## SMTP_PASSWORD
Password for the smtp user.

## DISABLE_REGISTRATION
- default: false

Whether to only allow admins to create accounts.

## SECRET_KEY
Secret key of this installation.

## DATABASE_TYPE
- default: sqlite3

One of:
- mysql
- postgres
- mssql
- sqlite3

## DATABASE_HOST
Database hostname. Only relevant if not using sqlite.

## DATABASE_NAME
- default: gitea or data/gitea.db (if using sqlite)

Database name. If using sqlite, this is a filepath.

## DATABASE_USER
Database username. Only relevant if not using sqlite.

## DATABASE_PASSWORD
Password for the database user. Only relevant if not using sqlite.

## LFS_JWT_SECRET
LFS authentication secret.

## LANDING_PAGE
- default: home

One of:
- home
- explore
- organizations

## DOMAIN
The domain name used in clone urls.

## PROTOCOL
- default: https

One of:
- http
- https

## PREFERRED_LICENSES
- default: Apache License 2.0,MIT License

Preferred Licenses to place at the top of the list.

Look up the names here: https://github.com/spdx/license-list

## SSH_PORT
The exposed ssh port.

## ROOT_URL
The url used to construct clone urls.
