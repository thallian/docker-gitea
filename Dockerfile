FROM golang:alpine as builder

ENV VERSION v1.4.2

RUN apk --no-cache add \
    git \
    make \
    gcc \
    musl-dev \
    sqlite-dev

WORKDIR /go/src

RUN go get -d -u code.gitea.io/gitea

WORKDIR /go/src/code.gitea.io/gitea

RUN git checkout $VERSION
RUN TAGS="bindata sqlite" make generate build

FROM registry.gitlab.com/thallian/docker-confd-env:master

COPY --from=builder /go/src/code.gitea.io/gitea/gitea /bin/gitea

ENV USER gitea
ENV HOME /home/gitea
ENV GITEA_WORK_DIR /home/gitea

RUN addgroup -g 2222 gitea
RUN adduser -h /home/gitea -S -D -u 2222 -G gitea gitea

RUN apk --no-cache add \
    git \
    git-lfs \
    sqlite \
    openssh-keygen

RUN mkdir -p /etc/gitea/ /home/gitea/data/sessions
RUN chown -R gitea:gitea /home/gitea/data/sessions

ADD /rootfs /

EXPOSE 2222 3000
VOLUME [ "/home/gitea/repositories/", "/home/gitea/data/lfs", "/home/gitea/data/attachments", "/home/gitea/data/ssh/", "/home/gitea/data/avatars" ]
